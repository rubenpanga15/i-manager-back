begin
    
    declare nbr integer default 0;

    select count(*) into nbr from commande;

    if (nbr is null OR nbr = 0) then
        set nbr = 1;
    else
        set nbr = nbr + 1;
    end if;

    set new.numero = lpad(nbr, 5, 0);
end

begin
    
    declare nbr integer default 0;

    select count(*) into nbr from facture;

    if (nbr is null OR nbr = 0) then
        set nbr = 1;
    else
        set nbr = nbr + 1;
    end if;

    set new.numero = lpad(nbr, 5, 0);
end