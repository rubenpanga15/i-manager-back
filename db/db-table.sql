create table site(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table user(
    id int(11) unsigned auto_increment primary key,
    username varchar(255) not null,
    `password` varchar(255) not null,
    noms varchar(255) not null,
    sexe varchar(1) not null,
    telephone varchar(25) not null,
    email varchar(255),
    adresse varchar(255),
    `role`varchar(255) not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table categorieArticle(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    status tinyint(1) not null default 1,
    fkSite int(1) unsigned not null,
    dateCreat datetime not null default now()
);

create table `table`(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table devise(
    code varchar(25) primary key,
    description varchar(255) not null,
    fkSite int(11) unsigned not null,
    isDefaultDevise tinyint(1) not null default 0,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table tarif(
    id int(11) unsigned auto_increment primary key,
    montant decimal(11,3) not null,
    fkDevise varchar(25) not null,
    fkSite int(11) unsigned not null,
    fkArticle int(11) unsigned not null,
    status tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table article(
    id int(11) unsigned auto_increment primary key,
    nom varchar(255) not null,
    description varchar(255) not null,
    code varchar(25) not null,
    fkCategorieArticle int(11) unsigned not null,
    uniteMesure varchar(255) not null,
    stockRestant decimal(11,3) not null,
    stockAlert decimal(11,3) not null,
    remise decimal(11, 3) not null default 0,
    remiseAPartirDe decimal(11, 3) default 0,
    isRemisePourcentage tinyint(1) not null default 0,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table fournisseur(
    id int(11) unsigned auto_increment primary key,
    noms varchar(255) not null,
    telephone varchar(25) not null,
    email varchar(255),
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table entreeStock(
    numero varchar(255) not null primary key,
    dateEntree datetime not null,
    fkFournisseur int(11) not null,
    fkUser int(11) unsigned not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table detailEntreeStock(
    id int(11) unsigned auto_increment primary key,
    fkArticle int(11) unsigned not null,
    fkEntreeStock varchar(25) not null,
    dateExpiration datetime not null,
    numeroSerie varchar(255) not null,
    volume decimal(11, 3) not null,
    prixUnitaire decimal(11, 3) not null,
    montantTotal decimal(11, 3) not null,
    isLot tinyint(1) not null default 0,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table sortieStock(
    numero varchar(255) not null primary key,
    dateEntree datetime not null,
    motif varchar(255) not null,
    fkUser int(11) unsigned not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table detailSortieStock(
    id int(11) unsigned auto_increment primary key,
    fkArticle int(11) unsigned not null,
    fkSortieStock varchar(25) not null,
    numeroSerie varchar(255) not null,
    volume decimal(11, 3) not null,
    isLot tinyint(1) not null default 0,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table mouvementStock(
    id int(11) unsigned auto_increment primary key,
    fkArticle int(11) unsigned not null,
    sens varchar(25) not null,
    volume decimal(11, 3) not null,
    valeur decimal(11, 3) not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table commande(
    numero varchar(25) primary key,
    fkUser int(11) unsigned not null,
    `table`varchar(25) not null,
    fkClient int(11) unsigned,
    client varchar(255),
    fkFacture varchar(25),
    montantTotal decimal(11, 3) not null,
    fkDefaultDevise varchar(25) not null,
    fkSite int(1) unsigned not null,
    etat varchar(255) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table detailCommande(
    id int(11) unsigned auto_increment primary key,
    fkCommande varchar(25) not null,
    fkArticle int(11) unsigned not null,
    volume decimal(11, 3) not null,
    prixUnitaire decimal(11, 3) not null,
    montant decimal(11, 3) not null,
    isAccompagnement tinyint(1) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table facture(
    numero varchar(25) primary key,
    fkUser int(11) unsigned not null,
    `table`varchar(25),
    fkClient int(11) unsigned,
    client varchar(255),
    montantTotal decimal(11, 3) not null,
    tva decimal(11,3) not null default 0,
    remise decimal(11, 3) not null,
    fkDefaultDevise varchar(25) not null,
    fkSite int(1) unsigned not null,
    isRemisePourcentage tinyint(1) not null default 1,
    applyTva tinyint(1) not null default 0,
    tauxDeChange decimal(11, 3) not null,
    etat varchar(255) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table detailFacture(
    id int(11) unsigned auto_increment primary key,
    fkFacture varchar(25) not null,
    fkArticle int(11) unsigned not null,
    volume decimal(11, 3) not null,
    prixUnitaire decimal(11, 3) not null,
    montant decimal(11, 3) not null,
    isAccompagnement tinyint(1) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table detailTaxeFacture(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    fkTaxe int(11) unsigned not null,
    fkFacture varchar(25) not null,
    montant decimal(11,3) not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table taxe(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    montant decimal(11,3) not null,
    isPourcentage tinyint(1) not null,
    fkSite int(11) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table paiement(
    id int(11) unsigned auto_increment primary key,
    fkFacture varchar(25) not null,
    client varchar(255),
    montantApayer decimal(11, 3) not null,
    montantPaye decimal(11, 3) not null,
    solde decimal(11, 3) not null,
    datePaiement datetime not null,
    fkDevise varchar(25) not null,
    fkUser decimal(11, 3) not null,
    fkSite int(1) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table configFature(
    id int(11) unsigned auto_increment primary key,
    remise decimal(11, 3) not null default 0,
    remiseAPartirDe decimal(11, 3) not null default 0,
    isPourcentage tinyint(1) not null default 1,
    fkSite int(11) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table tauxDeChange(
    id int(11) unsigned auto_increment primary key,
    fkDeviseFrom varchar(25) not null,
    fkDeviseTo varchar (25) not null,
    taux decimal(11,3) not null,
    fkSite int(11) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table taxe(
    id int(11) unsigned auto_increment primary key,
    description varchar(255) not null,
    montant decimal(11, 3) not null,
    isPourcentage tinyint(1) not null default 1,
    fkDevise varchar(25),
    fkSite int(11) unsigned not null,
    `status` tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

