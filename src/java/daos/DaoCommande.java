/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Article;
import entities.Commande;
import entities.DetailCommande;
import entities.DetailFacture;
import entities.Facture;
import entities.QueryParam;
import entities.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import others.Logger;

/**
 *
 * @author rubenpanga
 */
public class DaoCommande extends DaoGeneric {
    
    public String saveCommande(Commande commande){
        String numero = null;
        
        String sql = "insert into commande set "
                + "numero='', fkUser=:fkUser, `table`=:table, fkClient=:fkClient, client=:client,"
                + "montantTotal=:montantTotal, fkDefaultDevise=:fkDefaultDevise,"
                + "fkSite=:fkSite, etat='ATTENTE', status=1, dateCreat=now()";
        
        this.executeSQLQuery(sql, 
                new QueryParam("fkUser", commande.getFkUser()),
                new QueryParam("table", commande.getTable()),
                new QueryParam("fkClient", commande.getFkClient()),
                new QueryParam("client", commande.getClient()),
                new QueryParam("montantTotal", commande.getMontantTotal()),
                new QueryParam("fkDefaultDevise", commande.getFkDefaultDevise()),
                new QueryParam("fkSite", commande.getFkSite())
                );
        
        try{
            numero = this.getCommandes(commande.getFkUser()).get(0).getNumero();
        }catch(Exception e){}
        
        return numero;
    }
    
    public List<Commande> getCommandes(Integer fkUser){
        List<Commande> commandes = null;
        
        String hql = "from Commande where fkUser =:fk and status = 1 order by dateCreat desc";
        
        List<Commande> dataset = this.selectHQL(hql, new QueryParam("fk", fkUser));
        
        for(Commande commande: dataset){
            commande.setDetails(this.getDetailsCommande(commande.getNumero()));
            commande.setUser((User) this.findById(User.class, commande.getFkUser()));
            
            if(commandes == null)
                commandes = new ArrayList<>();
            commandes.add(commande);
        }
        
        return commandes;
    }
    
    public List<DetailCommande> getDetailsCommande(String fkCommande){
        List<DetailCommande> liste = null;
        
        String hql = "from DetailCommande where fkCommande =:fk";
        
        List<DetailCommande> dataset = this.selectHQL(hql, new QueryParam("fk", fkCommande));
        DaoConfiguration dao = new DaoConfiguration();
        for(DetailCommande detail: dataset){
            try{
                detail.setArticle(dao.getArticle(detail.getFkArticle()));
            }catch(Exception e){}
            
            if(liste == null)
                liste = new ArrayList<>();
            liste.add(detail);
        }
        
        return liste;
    }
    
    public List<Commande> getCommandes(String etat){
        List<Commande> liste = null;
        
        String hql = "from Commande where etat=:etat order by dateCreat desc";
        
        List<Commande> dataset = this.selectHQL(hql, new QueryParam("etat", etat));
        
        for(Commande commande: dataset){
            commande.setDetails(this.getDetailsCommande(commande.getNumero()));
            commande.setUser((User) this.findById(User.class, commande.getFkUser()));
            
            if(liste == null)
                liste = new ArrayList<>();
            liste.add(commande);
        }
        
        return liste;
    }
    
    public String saveFacture(Facture facture){
        String numero = null;
        
        Logger.printLog("TAUX DE CHANGE", facture.getTauxDeChange() + "");
        
        String sql = "insert into facture set "
                + "numero='', fkUser=:fkUser, `table`=:table, fkClient=:fkClient, client=:client,"
                + "montantTotal=:montantTotal, fkDefaultDevise=:fkDefaultDevise,"
                + "fkSite=:fkSite, etat='ATTENTE', status=1, dateCreat=now(), remise=:remise, "
                + "isRemisePourcentage=:isRemisePourcentage, applyTva=:applyTva, tva=:tva, tauxDeChange =:tauxDeChande";
        
        this.executeSQLQuery(sql, 
                new QueryParam("fkUser", facture.getFkUser()),
                new QueryParam("table", facture.getTable()),
                new QueryParam("fkClient", facture.getFkClient()),
                new QueryParam("client", facture.getClient()),
                new QueryParam("montantTotal", facture.getMontantTotal()),
                new QueryParam("fkDefaultDevise", facture.getFkDefaultDevise()),
                new QueryParam("fkSite", facture.getFkSite()),
                new QueryParam("remise", facture.getRemise()),
                new QueryParam("isRemisePourcentage", facture.getIsRemisePourcentage()),
                new QueryParam("applyTva", facture.getApplyTva()),
                new QueryParam("tva", facture.getTva()),
                new QueryParam("tauxDeChande", facture.getTauxDeChange())
                );
        
        try{
            numero = this.getFacture(facture.getFkUser()).get(0).getNumero();
        }catch(Exception e){}
        
        return numero;
    }
    
    
    public List<Facture> getFacture(Integer fkUser){
        List<Facture> commandes = null;
        
        String hql = "from Facture where fkUser =:fk and status = 1 order by dateCreat desc";
        
        List<Facture> dataset = this.selectHQL(hql, new QueryParam("fk", fkUser));
        
        for(Facture commande: dataset){
            commande.setDetails(this.getDetailsFacture(commande.getNumero()));
            commande.setUser((User) this.findById(User.class, commande.getFkUser()));
            
            if(commandes == null)
                commandes = new ArrayList<>();
            commandes.add(commande);
        }
        
        return commandes;
    }
    
    public Facture getFacture(String fkFacture){
        Facture facture = null;
        
        String hql = "from Facture where numero =:fk and status = 1 order by dateCreat desc";
        
        List<Facture> dataset = this.selectHQL(hql, new QueryParam("fk", fkFacture));
        
        if(dataset != null && dataset.size() > 0){
            facture = dataset.get(0);
            
            facture.setUser((User) this.findById(User.class, facture.getFkUser()));
            facture.setDetails(this.getDetailsFacture(facture.getNumero()));
            
        }
        
        return facture;
    }
    
    public List<DetailFacture> getDetailsFacture(String fkFacture){
        List<DetailFacture> liste = null;
        
        String hql = "from DetailFacture where fkFacture =:fk";
        
        List<DetailFacture> dataset = this.selectHQL(hql, new QueryParam("fk", fkFacture));
        DaoConfiguration dao = new DaoConfiguration();
        for(DetailFacture detail: dataset){
            try{
                detail.setArticle(dao.getArticle(detail.getFkArticle()));
            }catch(Exception e){}
            
            if(liste == null)
                liste = new ArrayList<>();
            liste.add(detail);
        }
        
        return liste;
    }
    
    public boolean updateEtatCommande(String fkCommande, String etat, String fkFacture){
        boolean done = false;
        
        String sql = "update Commande set etat =:etat, fkFacture=:fkFacture where numero =:numero";
        
        done = this.executeSQLQuery(sql, new QueryParam("etat", etat), new QueryParam("fkFacture", fkFacture), new QueryParam("numero", fkCommande));
        
        
        return false;
    }
    
    public Commande getCommande(String fkCommande){
        Commande commande = null;
        
        String hql = "from Commande where numero =:fk and status = 1 order by dateCreat desc";
        
        List<Commande> dataset = this.selectHQL(hql, new QueryParam("fk", fkCommande));
        
        if(dataset != null && dataset.size() > 0){
            commande = dataset.get(0);
            
            commande.setUser((User) this.findById(User.class, commande.getFkUser()));
            commande.setDetails(this.getDetailsCommande(commande.getNumero()));
            
        }
        
        return commande;
    }
    
    public List<Facture> getListeFactures(Date debut, Date fin, String etat){
        List<Facture> liste = null;
        
        String hql = "from Facture where (dateCreat between :debut and :fin) ";
        
        List<QueryParam> params = new ArrayList<>();
        params.add(new QueryParam("debut", debut));
        params.add(new QueryParam("fin", fin));
        
        if(etat != null){
            hql += "and etat =:etat";
            params.add(new QueryParam("etat", etat));
        }
        
        hql += " order by dateCreat desc";
        
        
        List<Facture> dataset = this.selectHQL(hql, params);
        
        for(Facture facture: dataset){
            facture.setUser((User) this.findById(User.class, facture.getFkUser()));
            facture.setDetails(this.getDetailsFacture(facture.getNumero()));
            
            if(liste == null)
                liste = new ArrayList<>();
            
            liste.add(facture);
        }
        
        return liste;
    }
    
    public List<Facture> getListeFactures(String etat){
        List<Facture> liste = null;
        
        String hql = "from Facture ";
        
        List<QueryParam> params = new ArrayList<>();
        
        if(etat != null){
            hql += "where etat <> :etat";
            params.add(new QueryParam("etat", etat));
        }
        
        hql += " order by dateCreat desc";
        
        
        List<Facture> dataset = this.selectHQL(hql, params);
        
        for(Facture facture: dataset){
            facture.setUser((User) this.findById(User.class, facture.getFkUser()));
            facture.setDetails(this.getDetailsFacture(facture.getNumero()));
            
            if(liste == null)
                liste = new ArrayList<>();
            
            liste.add(facture);
        }
        
        return liste;
    }
    
    public List<Commande> getListeCommandes(Date debut, Date fin, String etat){
        List<Commande> liste = null;
        
        String hql = "from Commande where dateCreat between :debut and :fin ";
        
        List<QueryParam> params = new ArrayList<>();
        params.add(new QueryParam("debut", debut));
        params.add(new QueryParam("fin", fin));
        
        if(etat != null){
            hql = "and etat =:etat";
            params.add(new QueryParam("etat", etat));
        }
        
        
        List<Commande> dataset = this.selectHQL(hql, params);
        
        for(Commande facture: dataset){
            facture.setUser((User) this.findById(User.class, facture.getFkUser()));
            facture.setDetails(this.getDetailsCommande(facture.getNumero()));
            
            if(liste == null)
                liste = new ArrayList<>();
            
            liste.add(facture);
        }
        
        return liste;
    }
    
    public boolean updateFactureState(String fkFacture, String etat){
        boolean done = false;
        
        String sql = "update Facture set etat =:etat where numero=:fk ";
        
        done = this.executeSQLQuery(sql, new QueryParam("etat", etat), new QueryParam("fk", fkFacture));
        
        return done;
    }
    
}
