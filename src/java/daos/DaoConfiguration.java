/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Article;
import entities.CategorieArticle;
import entities.ConfigFature;
import entities.Devise;
import entities.QueryParam;
import entities.Site;
import entities.Tarif;
import entities.TauxDeChange;
import entities.Taxe;
import entities.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rubenpanga
 */
public class DaoConfiguration extends DaoGeneric {
    
    public User connexion(String username, String password){
        User user = null;
        
        String hql = "from User where username =:username and password=:password and status = 1";
        
        List<User> list = this.selectHQL(hql, new QueryParam("username", username), new QueryParam("password", password));
        
        if(list != null && list.size() > 0){
            user = list.get(0);
            user.setSite((Site) findById(Site.class, user.getFkSite()));
        }
        
        return user;
    }
    
    public List<CategorieArticle> getListCategorieArticle(){
        List<CategorieArticle> liste = null;
        
        String hql = "from CategorieArticle where status <> 0";
        
        liste = this.selectHQL(hql);
        
        return liste;
    }
    
    public List<Article> getListeArticle(){
        List<Article> liste = null;
        
        String hql = "from Article where status <> 0";
        
        List<Article> dataset = this.selectHQL(hql);
        
        for(Article article : dataset){
            article.setCategorie((CategorieArticle) this.findById(CategorieArticle.class, article.getFkCategorieArticle()));
            article.setTarif(this.getTarif(article.getId()));
            
            if(liste == null)
                liste = new ArrayList<>();
            
            liste.add(article);
        }
        
        return liste;
    }
    
    public Article getArticle(Integer id){
        Article article = null;
        
        String hql = "from Article where id=:id";
        
        List<Article> data = this.selectHQL(hql, new QueryParam("id", id));
        
        if(data != null && data.size() > 0){
            article = data.get(0);
            article.setTarif(this.getTarif(article.getId()));
        }
        
        return article;
    }
    
    public Tarif getTarif(Integer fkArticle){
        Tarif tarif = null;
        
        String hql = "from Tarif where fkArticle =:fk";
        
        List<Tarif> data = this.selectHQL(hql, new QueryParam("fk", fkArticle));
        
        if(data != null && data.size() > 0){
            tarif = data.get(0);
            tarif.setDevise((Devise) this.findByCode(Devise.class, tarif.getFkDevise()));
        }
        
        return tarif;
    }
    
    public TauxDeChange getTauxDeChange(String fkDeviseFrom, String fkDeviseTo){
        TauxDeChange taux = null;
        
        String hql = "from TauxDeChange where fkDeviseFrom =:rom and fkDeviseTo =:to";
        
        List<TauxDeChange> data = this.selectHQL(hql, new QueryParam("rom", fkDeviseFrom), new QueryParam("to", fkDeviseTo));
        
        if(data != null || data.size() > 0){
            taux = data.get(0);
            
            taux.setFrom((Devise) this.findByCode(Devise.class, taux.getFkDeviseFrom()));
            taux.setTo((Devise) this.findByCode(Devise.class, taux.getFkDeviseTo()));
            
        }
        
        return taux;
    }
    
    public boolean setDefaultTaux(String fkDevise){
        boolean done = false;
        
        removeAllDefault();
        
        this.removeAllDefault();
        
        String sql = "update devise set isDefaultDevise = 1 where code =:code";
        
        done = this.executeSQLQuery(sql, new QueryParam("code", fkDevise));
        
        return done;
    }
    
    private void removeAllDefault(){
        String sql = "UPDATE devise SET isDefaultDevise = 0";
        this.executeSQLQuery(sql);
    }
    
    public Devise getDefaultDevise(){
        Devise devise = null;
        
        String hql = "from Devise where isDefaultDevise = 1";
        
        List<Devise> data = this.selectHQL(hql);
        
        if(data != null && data.size() > 0){
            devise = data.get(0);
        }
        
        return devise;
    }
    
    public List<TauxDeChange> getListTauxDeChange(){
        List<TauxDeChange> liste = null;
        
        String hql = "from TauxDeChange";
        
        List<TauxDeChange> data = this.selectHQL(hql);
        
        for(TauxDeChange taux: data){
            taux = getTauxDeChange(taux.getFkDeviseFrom(), taux.getFkDeviseTo());
            
            if(liste == null)
                liste = new ArrayList<>();
            liste.add(taux);
        }
        
        return liste;
    }
    
    public List<Taxe> getListTaxe(){
        List<Taxe> liste = null;
        
        String hql = "from Taxe";
        
        List<Taxe> data = this.selectHQL(hql);
        
        for(Taxe taxe: data){
            taxe.setDevise((Devise) this.findByCode(Devise.class, taxe.getFkDevise()));
            
            if(liste == null)
                liste = new ArrayList<>();
            liste.add(taxe);
        }
        
        return liste;
    }
    
    public ConfigFature getConfig(Integer fkSite){
        ConfigFature config = null;
        
        String hql = "from ConfigFature where fkSite =:fk";
        
        List<ConfigFature> data = this.selectHQL(hql, new QueryParam("fk", fkSite));
        
        if(data != null && data.size() > 0)
            config = data.get(0);
        
        return config;
    }
    
}
