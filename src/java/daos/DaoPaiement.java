/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Paiement;
import entities.QueryParam;
import entities.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rubenpanga
 */
public class DaoPaiement extends DaoGeneric{
    
    public List<Paiement> getPaiements(Date debut, Date fin){
        List<Paiement> paiements = null;
        
        String hql = "from Paiement where datePaiement between :debut and :fin order by dateCreat desc";
        
        List<Paiement> dataset = this.selectHQL(hql, new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        DaoCommande dao = new DaoCommande();
        for(Paiement paiement: dataset){
            paiement.setFacture(dao.getFacture(paiement.getFkFacture()));
            paiement.setUser((User) this.findById(User.class, paiement.getFkUser()));
            
            if(paiements == null)
                paiements = new ArrayList<>();
            paiements.add(paiement);
        }
        
        return paiements;
    }
    
}
