/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.DaoPaiement;
import entities.ErrorResponse;
import entities.Paiement;
import entities.ValueDataException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
public class PaiementController {
    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
    
    public PaiementController(){
        this.error = new ErrorResponse();
    }
    
   
    public Integer effectuerPaiement(Paiement paiement){
        Integer id = null;
        
        try{
            if(paiement == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            AppUtilities.controlValue(paiement.getFkFacture(), "Veuillez rensienger la facture");
            AppUtilities.controlValue(paiement.getFkDevise(), "Veuillez renseigner la devise");
            AppUtilities.controlValue(paiement.getMontantPaye(), "Veuillez renseigner le montant payé");
            AppUtilities.controlValue(paiement.getMontantApayer(), "Veuillez renseigner le montant à payer");
            //AppUtilities.controlValue((paiement.getSolde()), "Veuillez renseigner le solde");
            AppUtilities.controlValue(paiement.getFkSite(), "Veuillez renseigner le site");
            //AppUtilities.controlValue(paiement.getClient(), "Veuillez renseigner le client");
            
            DaoPaiement dao = new DaoPaiement();
            
            paiement.setDatePaiement(new Date());
            paiement.setDateCreat(new Date());
            
            paiement.setStatus(true);
            
            id = dao.save(paiement);
            
            if(id == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            String etat = "SOLDE";
            
            if(paiement.getSolde() > 0)
                etat = "NON-SOLDE";
            
            CommandeController commandeCtrl = new CommandeController();
            
            if(!commandeCtrl.updateFactureState(paiement.getFkFacture(), etat))
                throw new ValueDataException(commandeCtrl.getError().getErrorDescription());
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Paiement effectué avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return id;
    }
    
    public List<Paiement> getListPaiements(String dateDebut, String dateFin){
        List<Paiement> liste = null;
        
        try{
            AppUtilities.controlValue(dateDebut, "Veuillez renseigner la date de debut");
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            
            Date debut = null, fin = null;
            
            try{
                debut = sdf.parse(dateDebut);
                fin = sdf.parse(dateFin);
            }catch(Exception e){}
            
            if(debut == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            if(fin == null)
                fin = utilities.Utilities.transformEndDay(debut);
            
            DaoPaiement dao = new DaoPaiement();
            
            liste = dao.getPaiements(debut, fin);
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " paiement(s) trouvé(s)");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
}
