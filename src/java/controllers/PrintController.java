/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import entities.Commande;
import entities.ErrorResponse;
import entities.Facture;
import entities.ValueDataException;
import java.util.Base64;
import others.AppUtilities;
import utilities.AppConst;
import utilities.PrintUtility;

/**
 *
 * @author rubenpanga
 */
public class PrintController {
    
    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
    
    public PrintController(){
        this.error = new ErrorResponse();
    }
    
    public String printFacture(String fkFacture){
        String document = null;
        
        try{
            AppUtilities.controlValue(fkFacture, "Veuillez renseigner la facture");
            
            CommandeController ctrl = new CommandeController();
            
            Facture facture = ctrl.getFacture(fkFacture);
            
            if(facture == null)
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            
            byte[] data = PrintUtility.printFacture("", facture);
            
            if(data == null)
                throw new ValueDataException("Problème d'impression");
            
            document = Base64.getEncoder().encodeToString(data);
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return document;
    }
    
    public String printCommande(String fkCommande){
        String document = null;
        
        try{
            AppUtilities.controlValue(fkCommande, "Veuillez renseigner la commande");
            
            CommandeController ctrl = new CommandeController();
            
            Commande commande = ctrl.getCommande(fkCommande);
            
            if(commande == null)
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            
            byte[] data = PrintUtility.printComande("", commande);
            
            if(data == null)
                throw new ValueDataException("Problème d'impression");
            
            document = Base64.getEncoder().encodeToString(data);
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return document;
    }
    
}
