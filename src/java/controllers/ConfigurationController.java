/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.DaoConfiguration;
import entities.Article;
import entities.CategorieArticle;
import entities.ConfigFature;
import entities.Devise;
import entities.ErrorResponse;
import entities.Fournisseur;
import entities.Site;
import entities.Table;
import entities.Tarif;
import entities.TauxDeChange;
import entities.Taxe;
import entities.User;
import entities.ValueDataException;
import java.util.Date;
import java.util.List;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
public class ConfigurationController {

    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public ConfigurationController() {
        this.error = new ErrorResponse();
    }

    public User connexion(String username, String password) {
        User user = null;

        try {
            AppUtilities.controlValue(username, "Veuiller renseigner le nom d'utilisateur");
            AppUtilities.controlValue(password, "Veuillez renseigner le mot de passe");

            DaoConfiguration dao = new DaoConfiguration();

            user = dao.connexion(username, password);

            if (user == null) {
                throw new ValueDataException("Nom d'utilisateur ou mot de passe incorrect");
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return user;
    }
    
    public User editUser(User user) {

        try {
            if (user == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(user.getUsername(), "Veuillez renseigner le nom d'utilisateur");
            AppUtilities.controlValue(user.getPassword(), "Veuillez renseigner le mot de passe");
            AppUtilities.controlValue(user.getNoms(), "Veuillez renseigner le nom complet de l'utilisateur");
            AppUtilities.controlValue(user.getTelephone(), "Veuillez renseigner le numéro de téléphone de l'utilisateur");
            AppUtilities.controlValue(user.getRole(), "Veuillez renseigner le role de l'utilisateur");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (user.getId() != null) {
                if (dao.saveOrUpdate(user)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                user.setDateCreat(new Date());
                user.setStatus(true);

                Integer id = dao.save(user);
                user.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return user;
    }
    
    public List<User> getListUsers() {
        List<User> liste = null;

        try {
            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getList(User.class);

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " utilisateur(s) trouvé(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }

    public CategorieArticle editCategorie(CategorieArticle categorie) {

        try {
            if (categorie == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(categorie.getDescription(), "Veuillez renseigner la designation de la categorie");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (categorie.getId() != null) {
                if (dao.saveOrUpdate(categorie)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                categorie.setDateCreat(new Date());
                categorie.setStatus(true);

                Integer id = dao.save(categorie);
                categorie.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return categorie;
    }

    public List<CategorieArticle> getListCategorieArticle() {
        List<CategorieArticle> liste = null;

        try {
            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getListCategorieArticle();

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " Categorie(s) trouvée(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }

    public Devise editDevise(Devise devise) {

        try {
            if (devise == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(devise.getCode(), "Veuiller renseigner le code du devise");
            AppUtilities.controlValue(devise.getDescription(), "Veuiller renseigner la description du devise");

            DaoConfiguration dao = new DaoConfiguration();
            
            String code = null;

            if (devise.getCode() == null) {
                devise.setDateCreat(new Date());
                devise.setStatus(true);
                code = dao.saveReturnCode(devise);
            }else{
                if(dao.saveOrUpdate(devise)){
                    code = devise.getCode();
                }
            }
            
            if(devise.getIsDefaultDevise())
                dao.setDefaultTaux(code);

            
            if (code == null) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return devise;
    }
    
    public Devise getDefaultDevise() {
        
        Devise devise = null;

        try {

            DaoConfiguration dao = new DaoConfiguration();
            
            devise = dao.getDefaultDevise();
            
            if(devise == null){
                throw new ValueDataException("Aucune devise configuré par default");
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return devise;
    }

    public List<Devise> getDeviseListe() {
        List<Devise> liste = null;

        try {

            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getList(Devise.class);

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " devise(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }

    public Site editSite(Site site) {

        try {
            if (site == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(site.getDescription(), "Veuillez renseigner la designation du site");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (site.getId() != null) {
                if (dao.saveOrUpdate(site)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                site.setDateCreat(new Date());
                site.setStatus(true);

                Integer id = dao.save(site);
                site.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return site;
    }

    public List<Site> getListeSite() {
        List<Site> liste = null;

        try {

            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getList(Site.class);

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " site(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }
    
    public Fournisseur editFournisseur(Fournisseur fournisseur) {

        try {
            if (fournisseur == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(fournisseur.getNoms(), "Veuillez renseigner le nom du fournisseur");
            AppUtilities.controlValue(fournisseur.getTelephone(), "Veuillez renseigner le téléphone du fournisseur");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (fournisseur.getId() != null) {
                if (dao.saveOrUpdate(fournisseur)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                fournisseur.setDateCreat(new Date());
                fournisseur.setStatus(true);

                Integer id = dao.save(fournisseur);
                fournisseur.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return fournisseur;
    }
    
    public List<Fournisseur> getListeFournisseur() {
        List<Fournisseur> liste = null;

        try {

            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getList(Fournisseur.class);

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " Fournisseur(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }
    
    public Table editTable(Table table) {

        try {
            if (table == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(table.getDescription(), "Veuillez renseigner la designation de la table");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (table.getId() != null) {
                if (dao.saveOrUpdate(table)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                table.setDateCreat(new Date());
                table.setStatus(true);

                Integer id = dao.save(table);
                table.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return table;
    }

    public List<Table> getListTables() {
        List<Table> liste = null;

        try {
            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getList(Table.class);

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " table(s) trouvée(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }
    
    public Article editArticle(Article article) {

        try {
            if (article == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(article.getCode(), "Veuillez renseigner le code de l'article");
            AppUtilities.controlValue(article.getNom(), "Veuillez resneigner le nom e l'article");
            AppUtilities.controlValue(article.getFkCategorieArticle(), "Veuillez renseigner la categorie de l'article");
            AppUtilities.controlValue(article.getFkSite(), "Veuillez resneigner le site");
            //AppUtilities.controlValue(article.getRemise(), "Veuillez resneigner la remise");
            AppUtilities.controlValue(article.getStockRestant(), "veuillez renseigner le stock restant");
            AppUtilities.controlValue(article.getStockAlert(), "Veuillez renseigner le stock d'arlert");
            AppUtilities.controlValue(article.getUniteMesure(), "Veuiller renseigner l'untié de mesure");
            
            if(article.getTarif() == null){
                throw new ValueDataException("Veuiller renseigner le tarif");
            }
            
            AppUtilities.controlValue(article.getTarif().getMontant(), "Veuillez renseigner le prix unitaire de l'article");
            AppUtilities.controlValue(article.getTarif().getFkDevise(), "Veuilez renseigner la devise");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (article.getId() != null) {
                if (dao.saveOrUpdate(article)) {
                    editTarif(article.getTarif());
                    this.error.setErrorCode("OK");
                }
            } else {
                article.setDateCreat(new Date());
                article.setStatus(true);

                Integer id = dao.save(article);
                article.setId(id);

                if (id != null) {
                    article.getTarif().setFkArticle(id);
                    editTarif(article.getTarif());
                }
            }

        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return article;
    }
    
    public Tarif editTarif(Tarif tarif) {

        try {
            if(tarif == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            AppUtilities.controlValue(tarif.getFkArticle(), "Veuillez renseigner l'article du tarif");
            AppUtilities.controlValue(tarif.getFkDevise(), "Veuillez renseigner la devise du tarif");
            AppUtilities.controlValue(tarif.getFkSite(), "Veuillez renseigner le site du tarif");
            AppUtilities.controlValue(tarif.getMontant(), "Veuillez renseigner le montant du tarif");
            
            DaoConfiguration dao = new DaoConfiguration();
            
            if (tarif.getId() != null) {
                if (dao.saveOrUpdate(tarif)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                tarif.setDateCreat(new Date());
                tarif.setStatus(true);

                Integer id = dao.save(tarif);
                tarif.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return tarif;
    }
    
    public List<Article> getListArticles() {
        List<Article> liste = null;

        try {
            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getListeArticle();

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " article(s) trouvée(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return liste;
    }
    
    public TauxDeChange editTauxDeChange(TauxDeChange taux){
        
        try{
            if(taux == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            AppUtilities.controlValue(taux.getFkDeviseFrom(), "Veuillez renseigner la devise de depart");
            AppUtilities.controlValue(taux.getFkDeviseTo(), "Veuillez renseigner la devise cible");
            AppUtilities.controlValue(taux.getFkSite(), "Veuillez renseigner le site");
            AppUtilities.controlValue(taux.getTaux(), "Veuillez renseigner le taux");
            
            DaoConfiguration dao = new DaoConfiguration();
            
            if(!dao.saveOrUpdate(taux)){
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Taux de change modifié avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return taux;
    }
    
    public List<TauxDeChange> getListeTauxDeChange(){
        List<TauxDeChange> liste = null;
        
        try{
            
            DaoConfiguration dao = new DaoConfiguration();

            liste = dao.getListTauxDeChange();

            if (liste == null || liste.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " taux trouvé(s)");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public Taxe editTaxe(Taxe taxe){
        
        try {
            if (taxe == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            AppUtilities.controlValue(taxe.getDescription(), "Veuillez renseigner la designation du site");
            AppUtilities.controlValue(taxe.getMontant(), "Veuillez renseigner le montant");
            AppUtilities.controlValue(taxe.getFkSite(), "Veuillez renseigner le site");

            DaoConfiguration dao = new DaoConfiguration();

            this.error.setErrorCode("OK");

            if (taxe.getId() != null) {
                if (dao.saveOrUpdate(taxe)) {
                    this.error.setErrorCode("OK");
                }
            } else {
                taxe.setDateCreat(new Date());
                taxe.setStatus(true);

                Integer id = dao.save(taxe);
                taxe.setId(id);

                if (id != null) {
                    this.error.setErrorCode("OK");
                }
            }

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return taxe;
    }
    
    public List<Taxe> getListeTaxe(){
        List<Taxe> liste = null;
        
        try {
            
            DaoConfiguration dao = new DaoConfiguration();
            
            liste = dao.getListTaxe();
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + "taxe(s) trouvée(s)");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public ConfigFature editConfigFacture(ConfigFature config){
        
        try{
            if(config == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            DaoConfiguration dao = new DaoConfiguration();
            
            if(!dao.saveOrUpdate(config)){
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Configuration modifiée avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return config;
    }
    
    public ConfigFature getConfigFacture(Integer fkSite){
        ConfigFature config = null;
        
        try{
            AppUtilities.controlValue(fkSite, "Veuillez renseigner le site");
            
            DaoConfiguration dao = new DaoConfiguration();
            
            config = dao.getConfig(fkSite);
            
            if(config == null){
                throw new ValueDataException("Aucune configuration Trouvée");
            }
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Configuration trouvée");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return config;
    }
    
    public TauxDeChange getTaux(String fkDeviseFrom, String fkDeviseTo){
        TauxDeChange taux = null;
        
        try{
            
            AppUtilities.controlValue(fkDeviseFrom, "Veuillez renseigner la devise de depart");
            AppUtilities.controlValue(fkDeviseTo, "Veuillez renseigner la devise cible");
            
            DaoConfiguration dao = new DaoConfiguration();
            
            taux = dao.getTauxDeChange(fkDeviseFrom, fkDeviseTo);
            
            if(taux == null)
                throw new ValueDataException("Aucun taux configuré");
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Taux de change modifié avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return taux;
    }

}
