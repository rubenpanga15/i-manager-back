/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.DaoCommande;
import entities.Article;
import entities.Commande;
import entities.DetailCommande;
import entities.DetailFacture;
import entities.ErrorResponse;
import entities.Facture;
import entities.ValueDataException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
public class CommandeController {
    
    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public CommandeController() {
        this.error = new ErrorResponse();
    }
    
    public String saveCommande(Commande commande){
        String numero = null;
        
        try{
            if(commande == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            AppUtilities.controlValue(commande.getTable(), "Veuiller renseigner la table");
            AppUtilities.controlValue(commande.getFkDefaultDevise(), "Veuillez renseigner la devise par defaut");
            AppUtilities.controlValue(commande.getFkSite(), "Veuillez renseignr le site");
            AppUtilities.controlValue(commande.getMontantTotal(), "Veuiller renseigner le montant total");
            AppUtilities.controlValue(commande.getFkUser(), "Veuiller renseigner l'utilisateur");
            
            if(commande.getDetails() == null || commande.getDetails().size() <= 0)
                throw new ValueDataException("Aucun article envoyé");
            
            DaoCommande dao = new DaoCommande();
            
            numero = dao.saveCommande(commande);
            
            if(numero == null)
                throw new ValueDataException("La commande n'a pas été faite. Une erreur s'est produit lors de l'enregistrement");
            
            for(DetailCommande detail: commande.getDetails()){
                detail.setFkCommande(numero);
                detail.setStatus(true);
                detail.setDateCreat(new Date());
                
                Integer id = this.saveDetailCommande(detail);
                if(id == null)
                    throw new ValueDataException(this.error.getErrorDescription());
            }
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("La commande a été enregistrée avec succès. Le némuero est " + numero);
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return numero;
    }
    
    public String saveFacture(Facture facture){
        String numero = null;
        
        try{
            if(facture == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            //AppUtilities.controlValue(facture.getTable(), "Veuiller renseigner la table");
            AppUtilities.controlValue(facture.getFkDefaultDevise(), "Veuillez renseigner la devise par defaut");
            AppUtilities.controlValue(facture.getFkSite(), "Veuillez renseigner le site");
            AppUtilities.controlValue(facture.getMontantTotal(), "Veuiller renseigner le montant total");
            AppUtilities.controlValue(facture.getFkUser(), "Veuiller renseigner l'utilisateur");
            
            if(facture.getDetails() == null || facture.getDetails().size() <= 0)
                throw new ValueDataException("Aucun donnée envoyée");
            
            DaoCommande dao = new DaoCommande();
            
            numero = dao.saveFacture(facture);
            
            if(numero == null)
                throw new ValueDataException("La commande n'a pas été faite. Une erreur s'est produit lors de l'enregistrement");
            
            for(DetailFacture detail: facture.getDetails()){
                detail.setFkFacture(numero);
                detail.setStatus(true);
                detail.setDateCreat(new Date());
                
                Integer id = dao.save(detail);
                if(id == null)
                    throw new ValueDataException(AppConst.OPERATION_FAILED);
            }
            
            for(Commande commande: facture.getCommandes()){
                dao.updateEtatCommande(commande.getNumero(), "FACTURE", numero);
            }
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("La facture a été enregistrée avec succès. Le némuero est " + numero);
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return numero;
    }
    
    public Facture getFacture(String fkFacture){
        Facture facture = null;
        
        try{
            AppUtilities.controlValue(fkFacture, "Veuiller renseigner la facture");
            
            DaoCommande dao = new DaoCommande();
            
            facture = dao.getFacture(fkFacture);
            
            if(facture == null)
                throw new ValueDataException("Aucune facture retrouvé");
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return facture;
    }
    
    public List<Facture> getListeFactures(String dateDebut, String dateFin, String etat){
        List<Facture> liste = null;
        
        try{
            AppUtilities.controlValue(dateDebut, "veuillez renseigner la date de debut");
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            
            Date debut = sdf.parse(dateDebut);
            if(debut == null)
                throw new ValueDataException("Veuillez renseigner une date valide");
            Date fin = null;
            
            try{
                fin = sdf.parse(dateFin);
            }catch(Exception e){}
            
            if(fin == null)
                fin = sdf.parse(dateDebut);
            
            fin = utilities.Utilities.transformEndDay(fin);
            
            DaoCommande dao = new DaoCommande();
            
            liste = dao.getListeFactures(debut, fin, etat);
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " facture(s) trouvée(s)");
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public List<Commande> getListeCommandes(String dateDebut, String dateFin, String etat){
        List<Commande> liste = null;
        
        try{
            AppUtilities.controlValue(dateDebut, "veuillez renseigner la date de debut");
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            
            Date debut = sdf.parse(dateDebut);
            if(debut == null)
                throw new ValueDataException("Veuillez renseigner une date valide");
            Date fin = null;
            
            try{
                fin = sdf.parse(dateFin);
            }catch(Exception e){}
            
            if(fin == null)
                fin = sdf.parse(dateDebut);
            
            fin = utilities.Utilities.transformEndDay(fin);
            
            DaoCommande dao = new DaoCommande();
            
            liste = dao.getListeCommandes(debut, fin, etat);
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " commande(s) trouvée(s)");
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public Integer saveDetailCommande(DetailCommande detail){
        Integer id = null;
        
        try{
            
            if(detail == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            
            AppUtilities.controlValue(detail.getFkArticle(), "Veuillez renseigner l'article");
            AppUtilities.controlValue(detail.getVolume(), "Veuillez renseigner le volume");
            
            if(isStockNotSuffissant(detail.getFkArticle(), detail.getVolume()))
                throw new ValueDataException("Le stock est insuffisant pour cet article");
            
            if(!retirerStock(detail.getFkArticle(), detail.getVolume()))
                throw new ValueDataException("Impossible de retirer cet article du stock");
            
            DaoCommande dao = new DaoCommande();
            
            id = dao.save(detail);
            
            if(id == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return id;
    }
    
    private boolean isStockNotSuffissant(Integer fkArticle, Double volume){
        boolean isStockNotSuffissant = false;
        
        try{
            AppUtilities.controlValue(fkArticle, "Veuillez renseigner l'article");
            AppUtilities.controlValue(volume, "Veuillez renseigner le volume");
            
            DaoCommande dao = new DaoCommande();
            
            Article article = (Article) dao.findById(Article.class, fkArticle);
            if(article == null)
                throw new ValueDataException("Cet article n'est pas enregistré dans le système");
            
            if(article.getStockRestant() < volume)
                isStockNotSuffissant = true;
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return isStockNotSuffissant;
    }
    
    private boolean retirerStock(Integer fkArticle, Double volume){
        boolean isRetire = false;
        
        try{
            AppUtilities.controlValue(fkArticle, "Veuillez renseigner l'article");
            AppUtilities.controlValue(volume, "Veuillez renseigner le volume");
            
            DaoCommande dao = new DaoCommande();
            
            Article article = (Article) dao.findById(Article.class, fkArticle);
            if(article == null)
                throw new ValueDataException("Cet article n'est pas enregistré dans le système");
            
            article.setStockRestant(article.getStockRestant() - volume);
            
            if(dao.saveOrUpdate(article))
                isRetire = true;
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return isRetire;
    }
    
    public List<Commande> getCommandes(String etat){
        List<Commande> liste = null;
        
        try{
            AppUtilities.controlValue(etat, "Veuillez renseigner l'état");
            
            DaoCommande dao = new DaoCommande();
            
            liste = dao.getCommandes(etat);
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public Commande getCommande(String fkCommande){
        Commande commande = null;
        
        try{
            AppUtilities.controlValue(fkCommande, "Veuillez rensienger la commande");
            
            DaoCommande dao = new DaoCommande();
            
            commande = dao.getCommande(fkCommande);
            
            if(commande == null)
                throw new ValueDataException("Aucune commande ne porte ce numero");
            
            this.error.setErrorCode("OK");
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return commande;
    }
    
    public List<Facture> getListeFacture(String etat){
        List<Facture> liste = null;
        
        try{
            AppUtilities.controlValue(etat, "Veuillez renseigner l'état");
            
            DaoCommande dao = new DaoCommande();
            
            liste = dao.getListeFactures(etat);
            
            if(liste == null || liste.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(liste.size() + " facture(s) trouvée(s)");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return liste;
    }
    
    public boolean updateFactureState(String fkFacture, String etat){
        boolean done = false;
        
        try{
            AppUtilities.controlValue(fkFacture, "Veuillez renseigner la facture");
            
            DaoCommande dao = new DaoCommande();
            
            done = dao.updateFactureState(fkFacture, etat);
            
            if(!done)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription("Opération effectuée avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return done;
    }
    
}
