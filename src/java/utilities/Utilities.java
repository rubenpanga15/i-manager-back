/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Trinix
 */
public class Utilities {

    public static Date transformEndDay(Date d) {
        Date date = null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        cal.add(Calendar.HOUR, 23 - cal.get(Calendar.HOUR_OF_DAY));
        cal.add(Calendar.MINUTE, 59 - cal.get(Calendar.MINUTE));
        cal.add(Calendar.SECOND,59- cal.get(Calendar.SECOND));

        date = cal.getTime();

        return date;
    }

    public static String generateRandomString(int digits) {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder builder = new StringBuilder();
        while (digits-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        return builder.toString();
    }

    public static double arrondir(double a, double b) {
        return (double) ((int) (a * Math.pow(10, b) + .5)) / Math.pow(10, b);
    }

    public static double toTonne(double poids) {
        double tonne = 0;

        if (poids < 1000) {
            tonne = 1;
            return tonne;
        }

        long reserve = (long) poids / 1000;
        tonne += reserve;

        long substract = reserve * 1000;
        double reste = poids - substract;
        if (reste > 0) {
            tonne += 1;
        }

        return tonne;
    }

}
