/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import com.google.gson.Gson;
import entities.Commande;
import entities.DetailCommande;
import entities.DetailFacture;
import entities.Facture;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import others.Logger;

/**
 *
 * @author rubenpanga
 */
public class PrintUtility {

    public static byte[] printFacture(String path, Facture facture) {
        byte[] pdf = null;

        try {

            InputStream is = new FileInputStream(new File("/Users/rubenpanga/Netbeansprojects/imanager/src/java/design/facture.jrxml"));// PrintUtilities.class.getClass().getResourceAsStream("/ecollect/reports/rpt-declaration.jrxml");

            JasperDesign design = JRXmlLoader.load(is);

            JasperReport jr = JasperCompileManager.compileReport(design);
            JasperPrint jp = null;

            HashMap params = new HashMap();
            
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);

            params.put("PARAM_USER", facture.getUser().getNoms());
            params.put("TABLE", facture.getTable());
            params.put("CLIENT", facture.getClient());
            params.put("NUMERO", facture.getNumero());
            params.put("MONTANT_TOTAL", nf.format(calculMontantTotalFacture(facture.getDetails())) + " " + facture.getFkDefaultDevise());
            params.put("Taux", nf.format(facture.getTauxDeChange()));
            params.put("TOTAL_USD", nf.format(facture.getMontantTotal())+ " USD");
            params.put("TOTAL_CDF", nf.format(facture.getMontantTotal() * facture.getTauxDeChange()) + " FC");
            String remise = facture.getIsRemisePourcentage()? " %": facture.getFkDefaultDevise();
            remise = nf.format(facture.getRemise()) + " " + remise;
            params.put("REMISE", remise);
            params.put("TVA", nf.format(facture.getTva()) + " " + facture.getFkDefaultDevise());
            
            facture.setDetails(processDetailFacture(facture.getDetails()));

            jp = JasperFillManager.fillReport(jr, params, new JRBeanCollectionDataSource(facture.getDetails()));

            pdf = JasperExportManager.exportReportToPdf(jp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pdf;
    }
    
    private static Double calculMontantTotalFacture(List<DetailFacture> details){
        Double total = 0.0;
        
        for(DetailFacture detail: details){
            
            total += detail.getMontant();
            
        }
        
        return total;
    }
    
    private static Double calculMontantTotalCommande(List<DetailFacture> details){
        Double total = 0.0;
        
        for(DetailFacture detail: details){
            
            total += detail.getMontant();
            
        }
        
        return total;
    }
    
    public static byte[] printComande(String path, Commande facture) {
        byte[] pdf = null;

        try {

            InputStream is = new FileInputStream(new File("/Users/rubenpanga/Netbeansprojects/imanager/src/java/design/commande.jrxml"));// PrintUtilities.class.getClass().getResourceAsStream("/ecollect/reports/rpt-declaration.jrxml");

            JasperDesign design = JRXmlLoader.load(is);

            JasperReport jr = JasperCompileManager.compileReport(design);
            JasperPrint jp = null;

            HashMap params = new HashMap();
            
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            
            Logger.printLog("DETAIL", new Gson().toJson(facture.getDetails()));

            params.put("PARAM_USER", facture.getUser().getNoms());
            params.put("TABLE", facture.getTable());
            params.put("CLIENT", facture.getClient());
            params.put("NUMERO", facture.getNumero());
            
            //params.put("TOTAL_CDF", nf.format(facture.getMontantTotal() * facture.getTauxDeChange()));
            
            facture.setDetails(processDetailCommande(facture.getDetails()));

            jp = JasperFillManager.fillReport(jr, params, new JRBeanCollectionDataSource(facture.getDetails()));

            pdf = JasperExportManager.exportReportToPdf(jp);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pdf;
    }
    
    private static List<DetailFacture> processDetailFacture(List<DetailFacture> d){
        List<DetailFacture> details = new ArrayList<>();
        
        for(DetailFacture detail: d){
            detail.setDesignation(detail.getArticle().getNom());
            if(detail.getIsAccompagnement())
                detail.setDesignation(detail.getDesignation() +"(OFFERT)" );
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            detail.setQuantite(nf.format(detail.getVolume()));
            detail.setMontantUnitaire(nf.format(detail.getPrixUnitaire()) + " " + detail.getArticle().getTarif().getDevise().getCode());
            detail.setMontantTotal(nf.format(detail.getMontant())+ " " + detail.getArticle().getTarif().getDevise().getCode());
            
            details.add(detail);
        }
        
        return details;
    }
    
    public static List<DetailCommande> processDetailCommande(List<DetailCommande> d){
        List<DetailCommande> details = new ArrayList<>();
        
        for(DetailCommande detail: d){
            detail.setDesignation(detail.getArticle().getNom());
            if(detail.getIsAccompagnement())
                detail.setDesignation(detail.getDesignation() +"(OFFERT)" );
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            detail.setQuantite(nf.format(detail.getVolume()));
            detail.setMontantUnitaire(nf.format(detail.getPrixUnitaire()) + " " + detail.getArticle().getTarif().getDevise().getCode());
            detail.setMontantTotal(nf.format(detail.getMontant())+ " " + detail.getArticle().getTarif().getDevise().getCode());
            
            details.add(detail);
        }
        
        return details;
    }

}
