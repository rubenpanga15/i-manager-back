/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author ghost06
 */
public class AppConst {
    
    public static final String ERROR_UNKNOW = "iManager - Erreur inconnue survenue, contatez le support technique.";
    public static final String ERROR_SERVLET_UNKNOW = "iManager - Erreur inconnue au moment du traitement.";
    public static final String NO_PERMISSION = "iManager - Vous n'avez pas droit à effecuter cette action.";
    public static final String PARMS_EMPTY = "iManager - Aucun paramètre reçu.";
    public static final String PARAMS_INCORRECT_FORMAT ="iManager - Paramètres incorrectes.";
    public static final String AGENT_DOESNT_EXIST = "iManager - Votre compte utilisateur n'est relié à aucun agent.";
    public static final String OPERATION_FAILED = "iManager - L'opération n'a pas abouti.";
    public static final String EMPTY_LIST = "iManager - Aucune donnée retrouvée.";
    
}
