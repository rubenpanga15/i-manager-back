/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class Taxe {
    private Integer id;
    private String description;
    private Double montant;
    private Boolean isPourcentage;
    private String fkDevise;
    private Integer fkSite;
    private Boolean status;
    private Date dateCreat;
    
    private Devise devise;

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Boolean getIsPourcentage() {
        return isPourcentage;
    }

    public void setIsPourcentage(Boolean isPourcentage) {
        this.isPourcentage = isPourcentage;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public Integer getFkSite() {
        return fkSite;
    }

    public void setFkSite(Integer fkSite) {
        this.fkSite = fkSite;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
}
