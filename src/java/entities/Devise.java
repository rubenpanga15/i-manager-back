package entities;
// Generated 24 nov. 2020 15:58:39 by Hibernate Tools 4.3.1

import java.util.Date;

/**
 * Devise generated by hbm2java
 */
public class Devise implements java.io.Serializable {

    private String code;
    private String description;
    private Integer fkSite;
    private Boolean status;
    private Boolean isDefaultDevise;
    private Date dateCreat;

    public Boolean getIsDefaultDevise() {
        return isDefaultDevise;
    }

    public void setIsDefaultDevise(Boolean isDefaultDevise) {
        this.isDefaultDevise = isDefaultDevise;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFkSite() {
        return fkSite;
    }

    public void setFkSite(Integer fkSite) {
        this.fkSite = fkSite;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

}
