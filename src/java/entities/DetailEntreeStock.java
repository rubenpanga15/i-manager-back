package entities;
// Generated 24 nov. 2020 15:58:39 by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.Date;

/**
 * DetailEntreeStock generated by hbm2java
 */
public class DetailEntreeStock implements java.io.Serializable {

    private Integer id;
    private int fkArticle;
    private String fkEntreeStock;
    private Date dateExpiration;
    private String numeroSerie;
    private Double volume;
    private Double prixUnitaire;
    private Double montantTotal;
    private boolean isLot;
    private boolean status;
    private Date dateCreat;
    
    private Article article;

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public DetailEntreeStock() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFkArticle() {
        return fkArticle;
    }

    public void setFkArticle(int fkArticle) {
        this.fkArticle = fkArticle;
    }

    public String getFkEntreeStock() {
        return fkEntreeStock;
    }

    public void setFkEntreeStock(String fkEntreeStock) {
        this.fkEntreeStock = fkEntreeStock;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(Double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(Double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public boolean isIsLot() {
        return isLot;
    }

    public void setIsLot(boolean isLot) {
        this.isLot = isLot;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

}
