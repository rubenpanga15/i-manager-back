/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class DetailTaxeFacture {
    private Integer id;
    private String description;
    private Integer fkTaxe;
    private String fkFacture;
    private Double montant;
    private Boolean status;
    private Date dateCreat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFkTaxe() {
        return fkTaxe;
    }

    public void setFkTaxe(Integer fkTaxe) {
        this.fkTaxe = fkTaxe;
    }

    public String getFkFacture() {
        return fkFacture;
    }

    public void setFkFacture(String fkFacture) {
        this.fkFacture = fkFacture;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
}
