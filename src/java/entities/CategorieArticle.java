package entities;
// Generated 24 nov. 2020 15:58:39 by Hibernate Tools 4.3.1

import java.util.Date;

/**
 * CategorieArticle generated by hbm2java
 */
public class CategorieArticle implements java.io.Serializable {

    private Integer id;
    private String description;
    private Boolean status;
    private Integer fkSite;
    private Date dateCreat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getFkSite() {
        return fkSite;
    }

    public void setFkSite(Integer fkSite) {
        this.fkSite = fkSite;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    

}
