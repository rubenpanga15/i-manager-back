package entities;
// Generated 24 nov. 2020 15:58:39 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;

/**
 * Paiement generated by hbm2java
 */
public class Paiement  implements java.io.Serializable {


     private Integer id;
     private String fkFacture;
     private String client;
     private Double montantApayer;
     private Double montantPaye;
     private Double solde;
     private Date datePaiement;
     private String fkDevise;
     private Integer fkUser;
     private Integer fkSite;
     private Boolean status;
     private Date dateCreat;
     
     private Facture facture;
     private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkFacture() {
        return fkFacture;
    }

    public void setFkFacture(String fkFacture) {
        this.fkFacture = fkFacture;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Double getMontantApayer() {
        return montantApayer;
    }

    public void setMontantApayer(Double montantApayer) {
        this.montantApayer = montantApayer;
    }

    public Double getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(Double montantPaye) {
        this.montantPaye = montantPaye;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public Integer getFkUser() {
        return fkUser;
    }

    public void setFkUser(Integer fkUser) {
        this.fkUser = fkUser;
    }

    public Integer getFkSite() {
        return fkSite;
    }

    public void setFkSite(Integer fkSite) {
        this.fkSite = fkSite;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    

}


