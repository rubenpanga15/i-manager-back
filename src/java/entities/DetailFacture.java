/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class DetailFacture{
    private Integer id;
    private String fkFacture;
    private Integer fkArticle;
    private Double volume;
    private Double prixUnitaire;
    private Double montant;
    private Boolean isAccompagnement;
    private Boolean status;
    private Date dateCreat;
    
    private String designation;
    private String quantite;
    private String montantUnitaire;
    private String montantTotal;
    
    private Article article;

    public Article getArticle() {
        return article;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String getMontantUnitaire() {
        return montantUnitaire;
    }

    public void setMontantUnitaire(String montantUnitaire) {
        this.montantUnitaire = montantUnitaire;
    }

    public String getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(String montantTotal) {
        this.montantTotal = montantTotal;
    }
    
    

    public void setArticle(Article article) {
        this.article = article;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkFacture() {
        return fkFacture;
    }

    public void setFkFacture(String fkFacture) {
        this.fkFacture = fkFacture;
    }

    public Integer getFkArticle() {
        return fkArticle;
    }

    public void setFkArticle(Integer fkArticle) {
        this.fkArticle = fkArticle;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(Double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Boolean getIsAccompagnement() {
        return isAccompagnement;
    }

    public void setIsAccompagnement(Boolean isAccompagnement) {
        this.isAccompagnement = isAccompagnement;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
}
