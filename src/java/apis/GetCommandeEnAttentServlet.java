/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.CommandeController;
import entities.Commande;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "GetCommandeEnAttentServlet", urlPatterns = {"/api/commande/get-commande-attente"})
public class GetCommandeEnAttentServlet extends HttpServlet {
    private static final String TAG = GetCommandeEnAttentServlet.class.getSimpleName();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            
            CommandeController ctrl = new CommandeController();
            
            httpResponse.setResponse(ctrl.getCommandes("ATTENTE"));
            httpResponse.setError(ctrl.getError());
            
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
        
    }
}
