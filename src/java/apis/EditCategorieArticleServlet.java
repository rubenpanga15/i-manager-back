/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.ConfigurationController;
import entities.CategorieArticle;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "EditCategorieArticleServlet", urlPatterns = {"/api/configuration/edit-categorie"})
public class EditCategorieArticleServlet extends HttpServlet {
    private static final String TAG = EditCategorieArticleServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            CategorieArticle categorie = null;
            
            try{
                categorie = new Gson().fromJson(requestBody, new TypeToken<CategorieArticle>(){}.getType());
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            ConfigurationController ctrl = new ConfigurationController();
            
            httpResponse.setResponse(ctrl.editCategorie(categorie));
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            Logger.printLog(TAG, httpResponse.getError().getErrorDescription());
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            
            ConfigurationController ctrl = new ConfigurationController();
            
            httpResponse.setResponse(ctrl.getListCategorieArticle());
            httpResponse.setError(ctrl.getError());
            
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            Logger.printLog(TAG, httpResponse.getError().getErrorDescription());
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
