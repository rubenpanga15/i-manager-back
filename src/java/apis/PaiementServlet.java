/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.PaiementController;
import entities.Paiement;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "PaiementServlet", urlPatterns = {"/api/paiement/effectuer-paiement"})
public class PaiementServlet extends HttpServlet {
    private static final String TAG = PaiementServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            Paiement paiement = null;
            
            try{
                paiement = new Gson().fromJson(requestBody, new TypeToken<Paiement>(){}.getType());
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            PaiementController ctrl = new PaiementController();
            
            httpResponse.setResponse(ctrl.effectuerPaiement(paiement));
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
