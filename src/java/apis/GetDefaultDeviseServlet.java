/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apis;

import controllers.ConfigurationController;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "GetDefaultDeviseServlet", urlPatterns = {"/api/configuration/get-default-devise"})
public class GetDefaultDeviseServlet extends HttpServlet {
    private static final String TAG = GetDefaultDeviseServlet.class.getSimpleName();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();

        try {

            ConfigurationController ctrl = new ConfigurationController();

            httpResponse.setResponse(ctrl.getDefaultDevise());
            httpResponse.setError(ctrl.getError());

        } catch (Exception e) {
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        } finally {
            Logger.printLog(TAG, httpResponse.getError().getErrorDescription());
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
