
import java.text.SimpleDateFormat;
import java.util.Date;
import others.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rubenpanga
 */
public class Test {
    
    private static final String TAG = Test.class.getSimpleName();
    
    public static void main(String... args) {
        try {
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = sdf.format(new Date());
            Logger.printLog(TAG, dateString);
            
            Date date = utilities.Utilities.transformEndDay(sdf.parse(dateString));
            
            Logger.printLog(TAG, date.toString());
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Runtime.getRuntime().exit(0);
        }
    }
    
}
